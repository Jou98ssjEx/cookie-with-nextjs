import React from 'react'

import NextLink from 'next/link'
import MenuOutlinedIcon from '@mui/icons-material/MenuOutlined';
import { AppBar, IconButton, Link, Toolbar, Typography } from '@mui/material'

export const Navbar = () => {
  return (
    <AppBar
        position='sticky'
        elevation={ 0 }
    >
        <Toolbar>
            <IconButton
                size='large'
                edge='start'
                sx={{
                    mr: 1
                }}
            >
                <MenuOutlinedIcon />
            </IconButton>

            <NextLink 
                href={`/`} 
                passHref
            >
                <Link
                    underline='none'
                >
                    <Typography
                        variant='h6'
                        color={`white`}
                    >
                        Cookie Master
                    </Typography>
                </Link>
            </NextLink>

            <div 
                style={{
                    flex: 1
                }}
            />

            <NextLink 
                href={`/change-theme`} 
                passHref
            >
                <Link
                    underline='none'
                >
                    <Typography
                        variant='h6'
                        color={`white`}
                    >
                        Change Theme
                    </Typography>
                </Link>
            </NextLink>
        </Toolbar>
    </AppBar>
  )
}
