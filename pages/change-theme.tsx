import React, { ChangeEvent, FC, useState } from 'react'

import Cookies from 'js-cookie'
import { Card, CardContent, FormControl, FormControlLabel, FormLabel, Radio, RadioGroup } from '@mui/material'
import { Layouts } from '../components/layouts'

interface Props {
  theme: string,
}

const ChangeTheme: FC<Props> = ( {theme} ) => {

  // const { theme } = props;
  // console.log({props});

  const [currentTheme, setCurrentTheme] = useState(theme);

  const onChangeTheme = ( event: ChangeEvent <HTMLInputElement > ) => {
    const selectTheme = event.target.value;

    // Guardar cookies desde el front: tambien se puede manipular desde las devtool
    Cookies.set('theme', selectTheme);
    
    console.log(selectTheme);
    setCurrentTheme( selectTheme );
  }

  const handleClick = ( ) => {
    
  }


  return (
      <Layouts>
        <Card>
          <CardContent>
            <FormControl>
              <FormLabel> Theme </FormLabel>
                <RadioGroup
                  value={ currentTheme }
                  onChange={ onChangeTheme }
                  row
                  sx={{
                    mt:1
                  }}
                >
                  <FormControlLabel 
                    value={ `light`} 
                    control={ <Radio /> }
                    label={ `Light` }
                  /> 
                  <FormControlLabel 
                    value={ `dark`} 
                    control={ <Radio /> }
                    label={ `Dark` }
                  /> 
                  <FormControlLabel 
                    value={ `custom`} 
                    control={ <Radio /> }
                    label={ `Custom` }
                  /> 

                </RadioGroup>

            </FormControl>
          </CardContent>
        </Card>
      </Layouts>
  )
}


// You should use getServerSideProps when:
// - Only if you need to pre-render a page whose data must be fetched at request time
import { GetServerSideProps } from 'next'

export const getServerSideProps: GetServerSideProps = async ({ req }) => {

  const { theme = 'light' } = req.cookies;
  // const { data } = await  // your fetch function here 
  console.log({theme})
  const validTheme = ['light', 'dark', 'custom'];


  return {
    props: {
     theme : validTheme.includes( theme ) ? theme : 'light',
    }
  }
}

export default ChangeTheme