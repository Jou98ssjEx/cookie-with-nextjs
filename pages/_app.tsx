import '../styles/globals.css'
import {useState , useEffect} from 'react';
import type { AppContext, AppProps } from 'next/app'

import Cookie from 'js-cookie';
import { CssBaseline, ThemeProvider } from '@mui/material'
import { darkTheme, lightTheme, customTheme } from '../themes';

interface Props extends AppProps{
  theme: string,
}

function MyApp({ Component, pageProps, theme = 'dark' }: Props) {

  // console.log({rest});
  // const { theme } = rest

  const [currentTheme, setCurrentTheme] = useState(lightTheme);

  useEffect(() => {
    /**
     *  Todo esto se ejecuta en en front
     *  Para que la renderizacion se ejecute automaticamente:
     *  se debe crear un contexto
     */
    const cookieTheme = Cookie.get('theme');
    
    const currentTheme = ( cookieTheme === 'light') ? lightTheme
                                                 : ( cookieTheme === 'dark')
                                                     ? darkTheme
                                                     : customTheme

    setCurrentTheme( currentTheme );
  }, [])
  


  return (
    <ThemeProvider theme={ currentTheme } >
      <CssBaseline />
        <Component {...pageProps} />
    </ThemeProvider>
    )
}


// getInitalProp doc: https://nextjs.org/docs/api-reference/data-fetching/get-initial-props

/**
 *  Al utilizar el getInitalProp en el _app: todo el contenido va estar bajo demanda de solicitud
 *  va estar pre- rederizado
 *  y se desactiva el Static Props para todo, si estas en el _app,
 *  si esta en otro pagina solo se desactiva en ese lugar
 */

// MyApp.getInitialProps = async ( appContext: AppContext) => {

//   const { theme } = appContext.ctx.req ? ( appContext.ctx.req as any ).cookies : { theme: 'light'}

//   const validTheme = ['light', 'dark', 'custom'];

//   return{
//     theme: validTheme.includes( theme ) ? theme : 'light',
//   }

// }

export default MyApp
