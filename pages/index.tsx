import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import { Layouts } from '../components/layouts'
import styles from '../styles/Home.module.css'

const HomePage: NextPage = () => {
  return (
   <>
      <Layouts>
        
      </Layouts>
   </>
  )
}

export default HomePage
